#!/usr/bin/env python3

import os.path
import os
import sys
import subprocess
import shutil
import logging

logging.basicConfig(level=logging.INFO)

CONFIG_DIR = "/etc/nginx/jenkins"
NGINX_RELOAD_CMD = ["sudo", "service", "nginx", "reload"]
DOMAIN_SUFFIX = "sk.znick.ru"

NGINX_CONFIG_TEMPLATE = """
server {{
    root /var/www/jenkins/{name};

    server_name {name}.{domain};

}}
"""

def main():
    _, src_dir, dst_dir_prefix, name = sys.argv
    name = name.split("/")[-1]  # origin/branch_name -> branch_name
    dst_dir = os.path.join(dst_dir_prefix, name)
    config_path = os.path.join(CONFIG_DIR, name)

    logging.info("Make bench '%s' in dir '%s', config '%s'", name, dst_dir, config_path)

    if os.path.exists(dst_dir):
        shutil.rmtree(dst_dir)
    shutil.copytree(src_dir, dst_dir)
    subprocess.check_call(["chmod", "a+w", "-R", dst_dir])

    with open(config_path, "w") as config_fn:
        config_fn.write(NGINX_CONFIG_TEMPLATE.format(name=name, domain=DOMAIN_SUFFIX))

    subprocess.check_call(NGINX_RELOAD_CMD)

if __name__ == "__main__":
    main()
