Описание:
Скрипты автоматизации развертывания тестовых стендов (ангуляра = статических файлов) на сервер nginx через Jenkins 

1.
make_test_suite.py -> /var/lib/jenkins

2.
jenkins.sh - скрипт, который вбит в jenkins в формочку

3.
```jenkins ALL=(ALL) NOPASSWD: /usr/sbin/service nginx reload```
в /etc/sudoers, чтобы jenkins мог релоадить nginx

4.
```
mkdir -p /etc/nginx/jenkins
chown jenkins:jenkins /etc/nginx/jenkins
```
чтобы jenkins мог записывать сгенеренные конфиги

5.
В /etc/nginx/nginx.conf добавить в секцию http
```
        include /etc/nginx/jenkins/*;
        server_names_hash_bucket_size 128;
```
(второе нужно, чтобы можно было делать длинные имена вида http://6c86833a94c9a073711ceaa8401f89d92bbeff10.sk.znick.ru/ )

6.
```
mkdir -p /var/www/jenkins
chown jenkins:jenkins /var/www/jenkins
```
Сюда будут копироваться файлики из git-а.

7.
Как настроен jenkis запечатлено в pics.
(Создать Item -> Создать задачу со свободной конфигурацией)

8.
DNS:
```*.sk.znick.ru.    36000   IN      A       78.155.218.186```
