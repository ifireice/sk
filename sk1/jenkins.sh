DST_DIR_PREFIX=/var/www/jenkins

ref=${branch_or_tag}
[ "$revision" ] && ref=${revision}
[ -z "$name" ] && name=${ref}

git checkout ${ref}


~jenkins/make_test_suite.py . "$DST_DIR_PREFIX" "$name"
