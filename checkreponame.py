#!/usr/bin/env python3

import re
import time
import argparse
import subprocess
import shutil
import smtplib

MASK = re.compile("^(feature|bugfix)/task-\d+")
ALLOWED_BRANCH_NAME = "^(feature|bugfix)/task-\d+"
#MASK = re.compile(".*")
TTL = "1.minute"
EXCEPTION_BRANCHES = ["master"]
TEMPLATE_COMMIT = '''\
From: robot <no-reply@robots.com>
Subject: Lost branch: {giturl}, {branch}

{user}!
Last commit in {giturl}, branch {branch} was more than {x} ago.
Please merge or remove branch.
'''

TEMPLATE_RENAME = '''\
From: robot <no-reply@robots.com>
Subject: Rename branch: {giturl}, {branch}

{user}!
{giturl}, branch {branch} has bad name.
Name should be matched by regexp '{reg}'.
Please rename the branch.
'''

class CheckRepoName:

    def __init__(self, giturl):

        self.dirname = "tmp"
        self.giturl = giturl
        self.repo_clone()

    def __del__(self):
        shutil.rmtree(self.dirname)


    def repo_clone(self):
        subprocess.check_call(["git", "clone", "--bare", self.giturl, self.dirname])


    def get_branches(self):
        list_branch = subprocess.check_output(["git", "branch", "-a"], cwd=self.dirname)
        return filter(None, map(lambda x : x.strip("* ").strip(), list_branch.decode("utf-8").split("\n")))

    def check_branch_name(self, branch):
        return MASK.match(branch)

    def check_time_last_commit(self, branch):
        list_commit = subprocess.check_output(["git", "log", "--since={}".format(TTL), branch], cwd=self.dirname)
        if len(list_commit.decode("utf-8")) > 0:
            return True
        else:
            return False

    def get_last_commit(self, branch):
        user_email = subprocess.check_output(["git", "log", "-1", "--format=%cN <%cE>", branch], cwd=self.dirname).decode("utf-8").strip()
        return user_email

    def send_email(self, template, branch, to, giturl):
        print("send email", branch, to)
        text = template.format(user=to, x =TTL.replace(".", " "), giturl=giturl, branch=branch, reg=MASK)
        smtpObj = smtplib.SMTP_SSL('smtp.rambler.ru', 465)
        smtpObj.login('sk-test-2@rambler.ru', 'Au9haelooMeif3aegezi')
        smtpObj.sendmail("sk-test-2@rambler.ru", to, text)
        smtpObj.quit()
        time.sleep(2)


    def check(self):
        branches = self.get_branches()

        for branch in branches:
            if branch in EXCEPTION_BRANCHES:
                continue

            if self.check_branch_name(branch):
                if self.check_time_last_commit(branch):
                    continue
                else:
                    user_info = self.get_last_commit(branch)
                    self.send_email(TEMPLATE_COMMIT, branch, user_info, giturl)
            else:
                user_info = self.get_last_commit(branch)
                self.send_email(TEMPLATE_RENAME,branch, user_info, giturl)


if __name__== "__main__":
    parser = argparse.ArgumentParser(prog='checkreponame')
    parser.add_argument("--giturl", help='giturl', required=True)

    args = parser.parse_args()
    giturl = args.giturl

    repo = CheckRepoName(giturl)
    repo.check()